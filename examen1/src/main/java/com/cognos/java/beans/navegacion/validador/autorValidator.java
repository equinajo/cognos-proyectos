package com.cognos.java.beans.navegacion.validador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author JAVA
 */

@FacesValidator("autorValidator")
public class autorValidator implements Validator<String>{

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        if(t.length()!=0 && t.length()<3){
            FacesMessage msg = new FacesMessage("Error Autor", "No debe ser menor a 3 caracteres");
            throw new ValidatorException(msg);
        }
    }
    
}
