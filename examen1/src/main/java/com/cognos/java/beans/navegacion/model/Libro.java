/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.java.beans.navegacion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author equinajo
 */
@Named
@SessionScoped
public class Libro implements Serializable {
    
    private String isbn;
    private String autor;
    private String genero;
    private String idioma;
    private String anioPublicacion;
    private String paisPublicacion;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getAnioPublicacion() {
        return anioPublicacion;
    }

    public void setAnioPublicacion(String anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }

    public String getPaisPublicacion() {
        return paisPublicacion;
    }

    public void setPaisPublicacion(String paisPublicacion) {
        this.paisPublicacion = paisPublicacion;
    }
   
    public List<String> validar() {
        List<String> errores = new ArrayList<String>();
        
        if (autor.length()==0) {
            errores.add("Debe completar el campo Autor");
        }
        if (genero.length()==0) {
            errores.add("Debe completar el campo Genero");
        }
        if (idioma.length()==0) {
            errores.add("Debe completar el campo Idioma");
        }
        /*if (autor.length()!=0 && autor.length()<3) {
            errores.add("El campo Autor no debe ser menor a 3 caracteres");
        }
        if (genero.length()!=0 && genero.length()<3) {
            errores.add("El campo Genero no debe ser menor a 3 caracteres");
        }
        if (idioma.length()!=0 &&  idioma.length()<3) {
            errores.add("El campo Idioma no debe ser menor a 3 caracteres");
        }*/
        return errores;
    }
}
