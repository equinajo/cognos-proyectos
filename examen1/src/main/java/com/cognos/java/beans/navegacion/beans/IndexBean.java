/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.java.beans.navegacion.beans;

import com.cognos.java.beans.navegacion.model.Libro;
import com.cognos.java.beans.navegacion.model.Usuario;
import com.cognos.java.beans.navegacion.utils.Cttes;
import java.lang.reflect.Array;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Locale;
import javax.faces.context.FacesContext;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class IndexBean {

    @Inject
    Usuario usuario; //si no definimos ni public ni private, por defecto le pone package

    @Inject
    Libro libro;

    private List<String> errores;
    private List<String> erroresRegistro;
    private String success;

    public String procesar() {
        System.out.println("Ingresando al metodo");
        validar();
        System.out.println("errores" + errores.size());
        if (errores != null && errores.size() > 0) {

            if (errores.size() == 1 && errores.get(0).equals("credenciales")) {
                return Cttes.errorNav;
            } else {
                return null;
            }

        } else {
            libro = new Libro();
            return Cttes.perfilNav;
        }
    }

    private void validar() {
        errores = usuario.validar();
    }

    public String procesarRegistro() {
        System.out.println("Ingresando al metodo");
        validarRegistro();
        System.out.println("errores" + erroresRegistro.size());
        if (erroresRegistro != null && erroresRegistro.size() > 0) {
            return null;
        } else {
            success = "Se registro correctamente el Libro";
            return null;
        }
    }

    private void validarRegistro() {
        erroresRegistro = libro.validar();
    }

    public Usuario getPersona() {
        return usuario;
    }

    public void setPersona(Usuario persona) {
        this.usuario = persona;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }

    public List<String> getErroresRegistro() {
        return erroresRegistro;
    }

    public void setErroresRegistro(List<String> erroresRegistro) {
        this.erroresRegistro = erroresRegistro;
    }

    
    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
    
    public void cambiarIdiomaIngles() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("en"));
    }

    public void cambiarIdiomaFrances() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("pt"));
    }

    public void cambiarIdiomaEspaniol() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag(""));
    }
}
