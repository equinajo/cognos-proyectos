/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.java.beans.navegacion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@SessionScoped
public class Usuario implements Serializable {

    private String usuario;
    private String password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> validar() {
        List<String> errores = new ArrayList<String>();
        if (usuario.length() == 0) {
            errores.add("Debe completar el campo Nombre de Usuario");
        }
        if (password.length() == 0) {
            errores.add("Debe completar el campo Contraseña");
        }

        if (usuario.length() != 0 && password.length() != 0
                && ((!usuario.trim().toLowerCase().equals("admin@com.bo") && !password.trim().equals("admin"))
                || (usuario.trim().toLowerCase().equals("admin@com.bo") && !password.trim().equals("admin"))
                || (!usuario.trim().toLowerCase().equals("admin@com.bo") && password.trim().equals("admin")))) {
            errores.add("credenciales");
        }

        return errores;
    }
}
